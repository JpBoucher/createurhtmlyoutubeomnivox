﻿namespace Youtube_Omnivox
{
    partial class Form1
    {
        /// <summary>
        ///  Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        ///  Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        ///  Required method for Designer support - do not modify
        ///  the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnExcel = new System.Windows.Forms.Button();
            this.btnHtml = new System.Windows.Forms.Button();
            this.richTextBox1 = new System.Windows.Forms.RichTextBox();
            this.SuspendLayout();
            // 
            // btnExcel
            // 
            this.btnExcel.Location = new System.Drawing.Point(137, 241);
            this.btnExcel.Name = "btnExcel";
            this.btnExcel.Size = new System.Drawing.Size(322, 349);
            this.btnExcel.TabIndex = 0;
            this.btnExcel.Text = "Sélectionner le fichier Excel";
            this.btnExcel.UseVisualStyleBackColor = true;
            this.btnExcel.Click += new System.EventHandler(this.btnExcel_Click);
            // 
            // btnHtml
            // 
            this.btnHtml.Location = new System.Drawing.Point(515, 241);
            this.btnHtml.Name = "btnHtml";
            this.btnHtml.Size = new System.Drawing.Size(322, 349);
            this.btnHtml.TabIndex = 0;
            this.btnHtml.Text = "Créer les fichiers HTML";
            this.btnHtml.UseVisualStyleBackColor = true;
            this.btnHtml.Click += new System.EventHandler(this.btnHTML_Click);
            // 
            // richTextBox1
            // 
            this.richTextBox1.Location = new System.Drawing.Point(137, 27);
            this.richTextBox1.Name = "richTextBox1";
            this.richTextBox1.ReadOnly = true;
            this.richTextBox1.Size = new System.Drawing.Size(700, 175);
            this.richTextBox1.TabIndex = 1;
            this.richTextBox1.Text = "Application de création des fichiers HTML pour une rétroaction audiovidéo \n\n" +
                                     "1. Vous devez d'abord créer un fichier Excel qui contiendra les DA des étudiants dans la colonne A et les url" +
                                     " correspondants dans la colonne B. \n" +
                                     "2. Ensuite, il suffit de sélectionner le fichier Excel avec le bouton de gauche et de cliquer sur le bouton de " +
                                     "droit pour générer les fichiers html. Votre fichier Excel ne doit pas être ouvert durant l'exécution. \n\n" +
                                     "L'application créera un fichier html qui contient le lien vers la bonne réatroaction pour chaque étudiant. " +
                                     "Les fichiers seront créés dans le même dossier que le fichier Excel";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1100, 693);
            this.Controls.Add(this.richTextBox1);
            this.Controls.Add(this.btnHtml);
            this.Controls.Add(this.btnExcel);
            this.Name = "Form1";
            this.Text = "Form1";
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button btnExcel;
        private System.Windows.Forms.Button btnHtml;
        private System.Windows.Forms.RichTextBox richTextBox1;
    }
}

